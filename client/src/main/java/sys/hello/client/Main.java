package sys.hello.client;

import sys.hello.Hello;

public class Main {

    public static void main(String[] args) {
        Hello hello = new Hello();
        hello.say();
        hello.sayTwice();
        hello.sayThrice();
    }

}
